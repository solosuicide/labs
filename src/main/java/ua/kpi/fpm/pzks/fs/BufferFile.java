package ua.kpi.fpm.pzks.fs;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

public class BufferFile<T> extends Entity {
    private final static int MAX_BUF_FILE_SIZE = 10;
    private Queue<T> queue;
    private final Object monitor = new Object();

    private BufferFile(String name, Directory parent) {
        super(name, parent);
        queue = new LinkedList<>();
    }

    public static <T> BufferFile<T> create(String name, Directory parent) {
        return new BufferFile<>(name, parent);
    }

    public void push(T element) {
        synchronized (monitor) {
            if (queue.size() >= MAX_BUF_FILE_SIZE) {
                throw new IllegalCallerException("Too many elements in buffer");
            }
            queue.add(element);
        }
    }

    public T consume() {
        synchronized (monitor) {
            return queue.remove();
        }
    }

}
