package ua.kpi.fpm.pzks.fs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Buffer file")
public class BufferFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        var dir = Directory.create("some", null);

        var file = BufferFile.create("file sample", dir);
        assertNotNull(file);

        assertEquals("file sample", file.getName());

    }

    @Test
    @DisplayName("Consume and push")
    void consumePushTest() {
        var dir = Directory.create("dir", null);
        var file = BufferFile.<Character>create("file", dir);
        file.push('Q');
        file.push('A');
        assertEquals(Character.valueOf('Q'), file.consume());
        assertEquals(Character.valueOf('A'), file.consume());
    }

    @Test
    @DisplayName("Push many elements")
    void pushManyElementsTest() {
        var dir = Directory.create("dir", null);
        var file = BufferFile.<Character>create("file", dir);

        for (int i = 0; i < 10; i++) {
            file.push((char) ('A' + i));
        }
        assertThrows(IllegalCallerException.class, () -> file.push('Z'));
    }
}
